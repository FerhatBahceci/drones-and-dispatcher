
import dispatcher.and.drones.domain.drone.and.dispatcher.Dispatcher;
import dispatcher.and.drones.load.csv.LoadAndMapDataFromCsv;

import java.util.*;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        Dispatcher dispatcher = LoadAndMapDataFromCsv.getDispatcher();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Welcome to dispatcher and drones simulation! \nPress y if you wish to run the simulation. (y/n)");
        String userAnswer = scanner.next();

        if(userAnswer.equals("y")){
            dispatcher.run();
        }

        Arrays.stream(dispatcher.getDrones()).forEach(drone ->{
            LOGGER.info("\nDrone:  " + drone.getDroneId()
                    + "  receives shutdown signal from Dispatcher: " + dispatcher.getDispatcherId());
            try{
                Thread.sleep(3000);
            }catch(InterruptedException e){
                LOGGER.info(e.toString());
            }
        });
    }
}
