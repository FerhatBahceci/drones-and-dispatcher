package dispatcher.and.drones.mapper;

import dispatcher.and.drones.domain.drone.and.dispatcher.Drone;
import dispatcher.and.drones.domain.metrics.Coordinate;
import dispatcher.and.drones.domain.metrics.DronePosition;
import dispatcher.and.drones.domain.metrics.TubePosition;
import dispatcher.and.drones.domain.traffic.TrafficCondition;
import dispatcher.and.drones.domain.traffic.TrafficReport;

import java.sql.Timestamp;
import java.util.Random;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class Mapper {

    public static DronePosition mapDronePosition(String drone) {
        if (drone != null) {
            String[] droneState = splitBy(drone);
            return DronePosition.builder()
                    .withDroneId(Long.valueOf(droneState[0]))
                    .withCoordinate(mapCoordinate(castToDouble((trim((droneState[1])))), castToDouble((trim((droneState[2]))))))
                   .withTime(castToTimeStamp(trim(droneState[3])))
                    .build();
        }
        return null;
    }

    public static TubePosition mapTube(String tubePosition){
        if(tubePosition!=null){
            String [] tubePos= splitBy(tubePosition);
            return TubePosition.builder()
                    .withCoordinate(mapCoordinate(castToDouble((tubePos[1])), castToDouble(((tubePos[2])))))
                    .withTubeName(trim(tubePos[0]))
                    .build();
        }
        return null;
    }

    private static String [] splitBy(String string){
        return string!=null ? string.split(",")
                : null;
    }

    private static String trim(String string) {
        return string != null ? string.substring(1, string.length() - 1)
                : null;
    }

    private static Double castToDouble(String doubl) {
        return doubl != null ? Double.valueOf(doubl)
                : null;
    }

    private static Coordinate mapCoordinate(Double latitude, Double longitude) {
        return latitude != null ? Coordinate.builder()
                .withLatitude(latitude)
                .withLongitude(longitude)
                .build()
                : null;
    }

    private static Timestamp castToTimeStamp(String timeStamp) {
        return timeStamp!=null ? Timestamp.valueOf(timeStamp)
                : null;
    }
}

