package dispatcher.and.drones.domain.metrics;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class TubePosition {

    private Coordinate      coordinate;
    private String          tubeName;

    private TubePosition(Builder builder) {
        this.coordinate = builder.coordinate;
        this.tubeName = builder.tubeName;
    }

    private TubePosition(){
    }

    public String getTubeName() {
        return tubeName;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<TubePosition>{

        private Coordinate                  coordinate;
        private String                      tubeName;

        public Builder withCoordinate(Coordinate coordinate){
            this.coordinate = coordinate;
            return this;
        }

        public Builder withTubeName(String tubeName){
            this.tubeName = tubeName;
            return this;
        }

        @Override
        public TubePosition build() {
            return new TubePosition(this);
        }
    }
}
