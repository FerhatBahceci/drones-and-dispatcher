package dispatcher.and.drones.domain.drone.and.dispatcher;

import dispatcher.and.drones.domain.metrics.*;
import dispatcher.and.drones.domain.traffic.TrafficReport;
import java.util.Set;
import static dispatcher.and.drones.domain.drone.and.dispatcher.DroneServiceImpl.*;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class Drone implements Runnable{

    private CurrentPosition          currentPosition;
    private Long                     droneId;
    private Set<DronePosition>       droneRoadMapCache;
    private Double                   speed;
    private Set<TubePosition>        tubePositions;
    private Set<TrafficReport>       trafficReports;

    private Drone(Builder builder) {
        this.currentPosition = builder.currentPosition;
        this.droneId = builder.droneId;
        this.droneRoadMapCache = builder.droneRoadMapCache;
        this.speed = builder.speed;
        this.tubePositions = builder.tubePositions;
        this.trafficReports = builder.trafficReports;
    }

    private Drone(){}

    @Override
    public void run() {
        if(droneRoadMapCache!=null) {
            droneRoadMapCache.forEach(desPoint -> {
                locateTubeNearBy(this);
                this.speed = setSpeed(getCurrentPosition(), desPoint);
                this.currentPosition = travelToDestination(getCurrentPosition().getCurrentTime(), desPoint);
                this.droneRoadMapCache.remove(desPoint);
            });
        }
    }

    public Boolean sendDronePositionToCache(DronePosition dronePosition){
        if(droneRoadMapCache.size()<=10){
            droneRoadMapCache.add(dronePosition);
            return true;
        }
        return false;
    }

    private void reportTrafficCondition(TrafficReport trafficReport){
        if(trafficReport!=null && trafficReports!=null){
            this.trafficReports.add(trafficReport);
        }
    }

    private void locateTubeNearBy(Drone drone){
        Double droneAssesMaximumReach = 0.35;
        drone.getTubePositions().forEach(station ->{
            Double distanceFromInitialPoint = Double.valueOf(getDistance(drone.getCurrentPosition().getCoordinate(), station.getCoordinate()).toString());
            if(distanceFromInitialPoint <= droneAssesMaximumReach){
                TrafficReport trafficReport = makeTrafficReport(drone);
                reportTrafficCondition(trafficReport);
            }
        });
    }

    public Long getDroneId() {
        return droneId;
    }

    public Double getSpeed() { return speed; }

    public Set<TubePosition> getTubePositions() {
        return tubePositions;
    }

    public Set<DronePosition> getDroneRoadMapCache() {
        return droneRoadMapCache;
    }

    public CurrentPosition getCurrentPosition() {
        return currentPosition;
    }

    public Set<TrafficReport> getTrafficReports() {
        return trafficReports;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<Drone>{

        private CurrentPosition          currentPosition;
        private Long                     droneId;
        private Set<DronePosition>       droneRoadMapCache;
        private Double                   speed;
        private Set<TubePosition>        tubePositions;
        private Set<TrafficReport>       trafficReports;

        public Builder withCurrentPosition(CurrentPosition currentPosition){
            this.currentPosition = currentPosition;
            return this;
        }

        public Builder withDroneId(Long droneId){
            this.droneId = droneId;
            return this;
        }

        public Builder withSpeed(Double speed){
            this.speed = speed;
            return this;
        }

        public Builder withDroneRoadMapCache(Set<DronePosition> droneRoadMapCache){
            this.droneRoadMapCache = droneRoadMapCache;
            return this;
        }

        public Builder withTubePositions(Set<TubePosition> tubePositions){
            this.tubePositions = tubePositions;
            return this;
        }

        public Builder withTrafficReports(Set<TrafficReport> trafficReports){
            this.trafficReports = trafficReports;
            return this;
        }

        @Override
        public Drone build() {
            return new Drone(this);
        }
    }
}
