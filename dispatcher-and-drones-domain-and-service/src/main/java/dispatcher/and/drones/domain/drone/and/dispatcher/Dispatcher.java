package dispatcher.and.drones.domain.drone.and.dispatcher;

import dispatcher.and.drones.domain.metrics.DronePosition;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import static dispatcher.and.drones.domain.drone.and.dispatcher.DroneServiceImpl.*;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */ 

public class Dispatcher implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(Dispatcher.class.getName());

    private Long                         dispatcherId;
    private Drone[]                      drones;
    private List<DronePosition>          dronePositions;
    private Timestamp                    shutDownDronesTime;
    private Timestamp                    wakeDronesTime;
    private Instant                      time;

    private Dispatcher(Builder builder) {
        this.dronePositions = builder.dronePositions;
        this.dispatcherId = builder.dispatcherId;
        this.drones = builder.drones;
        this.shutDownDronesTime = builder.shutDownDronesTime;
        this.time = builder.time;
        this.wakeDronesTime = builder.wakeDronesTime;
    }

    private Dispatcher(){
    }

    @Override
    public void run() {
        getDronePositions().forEach(dronePosition -> {
            Arrays.stream(getDrones()).forEachOrdered(drone -> {
                if(allowedFlightTime(drone.getCurrentPosition().getCurrentTime(), getShutDownDronesTime(),getWakeDronesTime())
                     && authenticateDrone(drone.getDroneId(), dronePosition.getDroneId())
                     && drone.sendDronePositionToCache(dronePosition)){
                        drone.run();
                        showReports(drone);
                     }
            });
        });
    }

    public List<DronePosition> getDronePositions() {
        return dronePositions;
    }

    public static Builder builder(){
        return new Builder();
    }

    public Long getDispatcherId() {
        return dispatcherId;
    }

    public Drone[] getDrones() {
        return drones;
    }

    public Instant getTime() {
        return time;
    }

    public Timestamp getShutDownDronesTime() {
        return shutDownDronesTime;
    }

    public Timestamp getWakeDronesTime() {
        return wakeDronesTime;
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<Dispatcher>{

        private Long                        dispatcherId;
        private Drone[]                     drones;
        private List<DronePosition>          dronePositions;
        private Timestamp                   shutDownDronesTime;
        private Timestamp                   wakeDronesTime;
        private Instant                     time;

        public Builder withDronePositions(List<DronePosition>dronePositions){
            this.dronePositions = dronePositions;
            return this;
        }

        public Builder withDispatcherId(Long dispatcherId){
            this.dispatcherId = dispatcherId;
            return this;
        }

        public Builder withDrones(Drone... drones){
            this.drones = drones;
            return this;
        }

        public Builder withTime(Instant time){
            this.time = time;
            return this;
        }

        public Builder withShutDownDronesTime(Timestamp shutDownDrones){
            this.shutDownDronesTime = shutDownDrones;
            return this;
        }

        public Builder withWakeDronesTime(Timestamp wakeDrones){
            this.wakeDronesTime = wakeDrones;
            return this;
        }

        @Override
        public Dispatcher build() {
            return new Dispatcher(this);
        }
    }
}
