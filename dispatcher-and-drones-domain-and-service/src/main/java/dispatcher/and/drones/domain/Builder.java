package dispatcher.and.drones.domain;

/**
 * A builder interface used by anything that implements the builder pattern.
 *
 * = Usage
 *
 * [source,java]
 * ----
 * public class SomeClass{
 *
 *    private String attribute;
 *
 *    private SomeClass(AClassBuilder builder){*
 *    }
 *
 *    public static class SomeClassBuilder implements dispatcher.and.drones.domain.Builder<SomeClass>{
 *
 *        private String attribute;
 *
 *        public SomeClassBuilder withAttribute(String attribute){
 *            this.attribute = attribute;
 *            return this;
 *        }
 *
 *
 *        public SomeClass build(){
 *            new SomeClass(this);
 *        }
 *    }
 * }
 *
 *----
 *
 */
public interface Builder<T>
{
    T build();
}
