package dispatcher.and.drones.domain.metrics;

import java.sql.Timestamp;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class DronePosition implements Comparable<DronePosition> {

    private Coordinate          coordinate;
    private Long                droneId;
    private Timestamp           time;

    private DronePosition(Builder builder) {
        this.coordinate = builder.coordinate;
        this.droneId = builder.droneId;
        this.time = builder.time;
    }

    private DronePosition(){
    }

    public Long getDroneId() {
        return droneId;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Timestamp getTime() {
        return time;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public int compareTo(DronePosition o) {
        return getTime().compareTo(o.getTime());
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<DronePosition>{

        private Coordinate              coordinate;
        private Long                    droneId;
        private Timestamp               time;

        public Builder withDroneId(Long droneId){
            this.droneId = droneId;
            return this;
        }

        public Builder withCoordinate(Coordinate coordinate){
            this.coordinate = coordinate;
            return this;
        }

        public Builder withTime(Timestamp time){
            this.time = time;
            return this;
        }


        @Override
        public DronePosition build() {
            return new DronePosition(this);
        }
    }
}
