package dispatcher.and.drones.load.csv;

import dispatcher.and.drones.domain.drone.and.dispatcher.Dispatcher;
import dispatcher.and.drones.domain.drone.and.dispatcher.Drone;
import dispatcher.and.drones.domain.drone.and.dispatcher.DroneServiceImpl;
import dispatcher.and.drones.domain.metrics.Coordinate;
import dispatcher.and.drones.domain.metrics.CurrentPosition;
import dispatcher.and.drones.domain.metrics.DronePosition;
import dispatcher.and.drones.domain.metrics.TubePosition;
import dispatcher.and.drones.mapper.Mapper;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static dispatcher.and.drones.domain.drone.and.dispatcher.DroneServiceImpl.setTime;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class LoadAndMapDataFromCsv {

    public static Set<TubePosition> getTubePositions(){
        String [] tubesArray = CsvReader.loadFileFromPathConvertToStringArray("tube.csv");
        return  Arrays.stream(tubesArray).map(Mapper::mapTube).collect(Collectors.toSet());
    }

    public static List<DronePosition> getDronePosition1(){
        String [] drone1Array = CsvReader.loadFileFromPathConvertToStringArray("5937.csv");
        return Arrays.stream(drone1Array).map(Mapper::mapDronePosition).collect(Collectors.toList());
    }

    public static List<DronePosition> getDronePosition2(){
        String [] drone2Array = CsvReader.loadFileFromPathConvertToStringArray("6043.csv");
        return Arrays.stream(drone2Array).map(Mapper::mapDronePosition).collect(Collectors.toList());
    }

    public static Drone makeDrone(Long droneId, Set<TubePosition> tubePositions, CurrentPosition currentPosition){
        return tubePositions!=null || droneId!=null ?
                Drone.builder()
                        .withDroneId(droneId)
                        .withTubePositions(tubePositions)
                        .withDroneRoadMapCache(new HashSet<>())
                        .withCurrentPosition(currentPosition)
                        .withTrafficReports(new HashSet<>())
                        .withSpeed(40.0)
                        .build()
                : null;
    }

    public static Dispatcher getDispatcher(){

        final Timestamp SHUTDOWN = setTime(2018,3,22,8,10,0,0);

        final Timestamp WAKE_DRONES = setTime(2018,3,22,7,47,50,0);


        List<DronePosition> dronePositions = loadAndSortDronePositions(getDronePosition1(),getDronePosition2());

        Coordinate initialPos = Coordinate.builder()
                .withLatitude(51.464579)
                .withLongitude(-0.14)
                .build();

        CurrentPosition currentPosition1 = CurrentPosition.builder()
                .withCoordinate(initialPos)
                .withDroneId(5937L)
                .withCurrentTime(DroneServiceImpl.setTime(2018,3,22,7,47,54,0))
                .build();

        CurrentPosition currentPosition2 = CurrentPosition.builder()
                .withCoordinate(initialPos)
                .withDroneId(6043L)
                .withCurrentTime(DroneServiceImpl.setTime(2018,3,22,7,47,54,0))
                .build();


        Drone drone1 = LoadAndMapDataFromCsv.makeDrone(5937L, LoadAndMapDataFromCsv.getTubePositions(), currentPosition1);
        Drone drone2 = LoadAndMapDataFromCsv.makeDrone(6043L, LoadAndMapDataFromCsv.getTubePositions(),currentPosition2);

        return Dispatcher.builder()
                .withDronePositions(dronePositions)
                .withDispatcherId(1L)
                .withDrones(drone1,drone2)
                .withShutDownDronesTime(SHUTDOWN)
                .withWakeDronesTime(WAKE_DRONES)
                .withTime(DroneServiceImpl.setTime(2018,03,22,7,47,53,0).toInstant())
                .build();
    }

    @SafeVarargs
    private static List<DronePosition> loadAndSortDronePositions(List<DronePosition>... dronePositions){
        if(dronePositions!=null){
            List<DronePosition> out = new ArrayList<>();
            Arrays.stream(dronePositions).forEach(out::addAll);
            Collections.sort(out);
            return out;
        }
        return null;
    }

}
