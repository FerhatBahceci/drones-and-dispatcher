package dispatcher.and.drones.domain.metrics;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class CurrentPosition {

    private Coordinate      coordinate;
    private Timestamp       currentTime;
    private Long            droneId;


    public CurrentPosition(Builder builder) {
        this.coordinate = builder.coordinate;
        this.droneId = builder.droneId;
        this.currentTime = builder.currentTime;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Long getDroneId() {
        return droneId;
    }

    public Timestamp getCurrentTime() {
        return currentTime;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder implements  dispatcher.and.drones.domain.Builder<CurrentPosition>{

        private Coordinate          coordinate;
        private Timestamp           currentTime;
        private Long                droneId;

        public Builder withDroneId(Long droneId){
            this.droneId = droneId;
            return this;
        }

        public Builder withCoordinate(Coordinate coordinate){
            this.coordinate = coordinate;
            return this;
        }

        public Builder withCurrentTime(Timestamp currentTime){
            this.currentTime = currentTime;
            return this;
        }

        @Override
        public CurrentPosition build() {
            return new CurrentPosition(this);
        }
    }
}
