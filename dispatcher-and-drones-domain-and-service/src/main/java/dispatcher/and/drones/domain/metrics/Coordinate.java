package dispatcher.and.drones.domain.metrics;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class Coordinate {

    private Double      latitude;
    private Double      longitude;

    private Coordinate(Builder builder) {
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
    }

    private Coordinate(){
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<Coordinate>{

        private Double          latitude;
        private Double          longitude;

        public Builder withLatitude(Double latitude){
            this.latitude = latitude;
            return this;
        }

        public Builder withLongitude(Double longitude){
            this.longitude = longitude;
            return this;
        }

        @Override
        public Coordinate build() {
            return new Coordinate(this);
        }
    }
}
