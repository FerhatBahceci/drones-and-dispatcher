package dispatcher.and.drones.domain.drone.and.dispatcher;

import dispatcher.and.drones.domain.metrics.Coordinate;
import dispatcher.and.drones.domain.metrics.CurrentPosition;
import dispatcher.and.drones.domain.metrics.DistanceCalculator;
import dispatcher.and.drones.domain.metrics.DronePosition;
import dispatcher.and.drones.domain.traffic.TrafficCondition;
import dispatcher.and.drones.domain.traffic.TrafficReport;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class DroneServiceImpl {

    private final static Logger LOGGER = Logger.getLogger(DroneServiceImpl.class.getName());

    public static CurrentPosition travelToDestination(Timestamp time, DronePosition dronePosition){
        if(dronePosition!=null){
            while(dronePosition.getTime().before(time)){
                LOGGER.info("\n\nDrone moving against tube stations!\n");
                time = dronePosition.getTime();
            }
            LOGGER.info("\n\n\nDrone " + dronePosition.getDroneId()+ " has reached assesed a tube station nearby:"
                    + "\nLatitude: " +  dronePosition.getCoordinate().getLatitude()
                    + "\nLongitude: " + dronePosition.getCoordinate().getLongitude()
                    + "\nTime: "+ dronePosition.getTime() + "\n\n");

            return CurrentPosition.builder()
                    .withCoordinate(dronePosition.getCoordinate())
                    .withDroneId(dronePosition.getDroneId())
                    .withCurrentTime(dronePosition.getTime())
                    .build();
        }
        return null;
    }

    public static void showReports(Drone drone) {
        if(drone!=null && drone.getTrafficReports().size()>0) {
            drone.getTrafficReports().forEach(report -> {
                String reportLog = "TrafficReport from Drone:" + report.getDroneId()
                        + "\nWith speed:" + report.getSpeed().toString() + "m/s"
                        + "\nTime:" + report.getTime().toString()
                        + "\nWith the current trafficCondition:" + report.getTrafficCondition().toString() +"\n\n";
                LOGGER.info(reportLog);
            });
        }
    }

    public static TrafficReport makeTrafficReport(Drone drone){
        return drone!= null ? TrafficReport.builder()
                .withDroneId(drone.getDroneId())
                .withSpeed(drone.getSpeed())
                .withTime(drone.getCurrentPosition().getCurrentTime())
                .withTrafficCondition(getRandomTrafficCondition())
                .build() : null;
    }

    private static TrafficCondition getRandomTrafficCondition(){
        Integer randomInt = new Random().nextInt(TrafficCondition.values().length);
        return TrafficCondition.values()[randomInt];
    }

    public static Double  getDistance(Coordinate currentPos, Coordinate destPos){
        if(currentPos!=null && destPos!=null){
            return DistanceCalculator.distance(currentPos.getLatitude(), currentPos.getLongitude(), destPos.getLatitude(), destPos.getLongitude(),"K");
        }
        return null;
    }

    private static Duration getHour(Timestamp t1, Timestamp t2){
        if(t1!=null && t2!=null) {
            return Duration.between(t1.toInstant(), t2.toInstant());
        }
        return null;
    }

    public static Double setSpeed(CurrentPosition currentPosition, DronePosition destinationPosition){
        if(currentPosition!=null && destinationPosition!=null) {
            Double distanceBetweenPoints = getDistance(currentPosition.getCoordinate(), destinationPosition.getCoordinate());
            Duration travelingTime = getHour(destinationPosition.getTime(), currentPosition.getCurrentTime());
            return (distanceBetweenPoints*10000 / travelingTime.getSeconds());
        }
        return null;
    }

    public static Timestamp setTime(Integer year, Integer month, Integer day,
                                     Integer hour, Integer minute, Integer second, Integer ms) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, ms);

        return  new Timestamp(cal.getTimeInMillis());
    }

    public static Boolean allowedFlightTime(Timestamp time, Timestamp shutdown, Timestamp awake){
        return time != null && (!time.after(shutdown) && !time.before(awake));
    }

    public static Boolean authenticateDrone(Long droneId, Long droneIdInPosition){
        return (droneId != null || droneIdInPosition != null) && Objects.equals(droneId, droneIdInPosition);
    }
}
