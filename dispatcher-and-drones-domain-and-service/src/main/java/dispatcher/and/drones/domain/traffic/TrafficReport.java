package dispatcher.and.drones.domain.traffic;

import java.sql.Timestamp;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public class TrafficReport {

    private Long                    droneId;
    private Double                  speed;
    private Timestamp               time;
    private TrafficCondition        trafficCondition;

    private TrafficReport(Builder builder){
        this.droneId = builder.droneId;
        this.speed = builder.speed;
        this.time = builder.time;
        this.trafficCondition = builder.trafficCondition;
    }

    private TrafficReport(){
    }

    public Long getDroneId() {
        return droneId;
    }

    public Double getSpeed() {
        return speed;
    }

    public Timestamp getTime() {
        return time;
    }

    public TrafficCondition getTrafficCondition() {
        return trafficCondition;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder implements dispatcher.and.drones.domain.Builder<TrafficReport>{

        private Long                    droneId;
        private Double                  speed;
        private Timestamp                 time;
        private TrafficCondition        trafficCondition;


        public Builder withDroneId(Long droneId){
            this.droneId = droneId;
            return this;
        }

        public Builder withSpeed(Double speed){
            this.speed = speed;
            return this;
        }

        public Builder withTime(Timestamp time){
            this.time = time;
            return this;
        }

        public Builder withTrafficCondition(TrafficCondition trafficCondition){
            this.trafficCondition = trafficCondition;
            return this;
        }


        @Override
        public TrafficReport build() {
            return new TrafficReport(this);
        }
    }
}


