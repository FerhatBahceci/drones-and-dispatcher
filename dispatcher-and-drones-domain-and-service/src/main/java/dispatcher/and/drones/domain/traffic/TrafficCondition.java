package dispatcher.and.drones.domain.traffic;

/**
 * @author Ferhat Bahceci {@literal <mailto:ferhat.bahceci@so4it.com/>}
 */
public enum TrafficCondition {
    HEAVY,
    LIGHT,
    MODERATE
}
