package dispatcher.and.drones.load.csv;

import dispatcher.and.drones.domain.drone.and.dispatcher.Dispatcher;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import java.io.*;
import java.util.logging.Logger;

public class CsvReader {

    private static final Logger LOGGER = Logger.getLogger(Dispatcher.class.getName());

    private static final String ENCODING = "UTF-8";

    private static final String SPLIT_BY = "\n";

    public static String [] loadFileFromPathConvertToStringArray(String path) {
        try (InputStream inputStream = new ClassPathResource(path).getInputStream()) {
            return IOUtils.toString(inputStream, ENCODING).split(SPLIT_BY);
        } catch (IOException e) {
            LOGGER.info(e.toString());
        }
        throw new RuntimeException(String.format("Failed loading resource. path=%s", path));
    }
}